<?php

namespace User\Middleware;

class Logging {
    public function __invoke($request,$response,$next){
    //befor route
    error_log($request->getMethod()."--".$request->getUri()."\r\n",3,"log.txt");
    $response = $next($request,$response);
    
    //after route
    return $response->withHeader('Access-Control-Allow-Origin', '*')->withHeader('Access-Control-Allow-Methods', '*');
    }
}