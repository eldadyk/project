import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {
  // Variables
  httpClient:HttpClient;
  http:Http;
  token;
  user_id;
  headers: Headers;

  getUsers(){
    //get messages from the SLIM rest API (No DB)
    //טוקן של כל בקשה להתחברות, אישור לקבלת נתונים
     let token = localStorage.getItem('token');
     let options = {
       headers: new Headers({
         'Authorization':'Bearer '+token
       })
     }
    return this.http.get(environment.url + '/users',options);
   }


   getUser(id){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
     return this.http.get(environment.url + '/users/'+ id, options);
  }
  

  postUser(data){       
    var headers = new HttpHeaders();    
    headers.append('Content-Type', 'application/form-data');


    return this.httpClient.post(environment.url + '/users/create', data, { headers: headers });
  }

  constructor(httpClient:HttpClient,http:Http ) {
  	this.http    = http;
    this.httpClient = httpClient;
  	this.token   = localStorage.getItem('token');
  	this.user_id = localStorage.getItem('user_id');
    this.headers = new Headers();
    this.headers.set('Content-Type', 'application/x-www-form-urlencoded');
  }
  
  login(credentials){
    let options = {
       headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
       })
    }
   let  params = new HttpParams().append('username', credentials.username).append('password',credentials.password);
   
   return this.http.post(environment.url + '/login', params.toString(),options);
 }

}
