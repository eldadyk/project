import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class MessagesService {

  // Variables
  http:Http;
  items;

  constructor(http:Http) { 
    this.http = http; 
  }

  getMessages(){  
	  return this.http.get(environment.url + '/messages');
  }

  getMessage(id){
    let token = localStorage.getItem('token');
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
     return this.http.get(environment.url + '/messages/'+ id, options);
  }

  postMessage(data){
    let options =  {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    console.log(data.user_id);
    let params = new HttpParams().append('body', data.body).append('title', data.title).append('user_id', data.user_id);    
    return this.http.post(environment.url + '/messages' , params.toString(),options);
  }

  deleteMessage(key){
    return this.http.delete(environment.url + '/messages/' + key);
  }

  getMessageByUser(user_id){
    return this.http.get(environment.url + '/messages_by_user/' + user_id); 
  }

  setLike(key,user_id){
    return this.items.push({message_id: key, user_id: user_id});
  }


  deleteLike(key){
    return this.items.remove(key); 
  }

}
