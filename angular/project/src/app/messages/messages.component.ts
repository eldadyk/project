import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';
import { Router } from "@angular/router";
import * as JWT from 'jwt-decode';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  
  messages;
  messagesKeys =[];  
  token;
  user_id;
  

  optimisticAdd(message){
    //console.log("addMessage work "+message); בדיקה
    var newKey = parseInt(this.messagesKeys[this.messagesKeys.length - 1],0) + 1;
    var newMessageObject = {};
    newMessageObject['body'] = message; //גוף ההודעה עצמה
    this.messages[newKey] = newMessageObject;
    this.messagesKeys = Object.keys(this.messages);
  }
  pasemisticAdd(){
	  this.service.getMessages().subscribe(response=>{      
	      this.messages = response.json();
	      this.messagesKeys = Object.keys(this.messages);  
        let count = 0;      
        let key = '';
        this.messagesKeys.forEach(element => {
            this.messages[element].liked = false;          
           
        });   
	  });    
  }  

  like(key){    
    this.service.setLike(key, this.user_id);
  }

  dislike(key, id){
    this.service.deleteLike(key);
    this.messages[id].liked = false;  
  }

  constructor(private service:MessagesService, private router:Router, private spinnerService: Ng4LoadingSpinnerService) {   
  	this.token = localStorage.getItem('token');
    this.user_id = localStorage.getItem('user_id');    
    this.spinnerService.show();
  	
    service.getMessages().subscribe(response=>{      
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);      
      let count = 0;      
      let key = '';
      this.messagesKeys.forEach(element => {
          this.messages[element].liked = false;          
         
      });
      this.spinnerService.hide();      
  	});
  }

  ngOnInit() {  	
  	
  }

}
